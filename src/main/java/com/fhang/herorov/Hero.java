/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fhang.herorov;

/**
 *
 * @author Natthakritta
 */
public class Hero {

    private String name;
    private char type;
    private int blood = 2500;
    private int Attack = 0;
    private int sumAttack = 0;

    public Hero(String name, char type) {
        this.name = name;
        this.type = type;
    }
    public void show(){
        System.out.println("Name = "+name+" ,Type = "+type);
        System.out.println("Blood = "+blood+" ,sumAttack = "+sumAttack);
        
    }

    public void setAttack(int Attack) {
        this.Attack = Attack;
        if (blood<=0) {
            System.out.println("error!!!!");
        }else{
            blood = blood-Attack;
            sumAttack(Attack);
            if (blood<=0) {
                System.out.println(name+" die!!!");
            }
        }
    }

    public void sumAttack(int Attack) {
        sumAttack = sumAttack + Attack;
    }

    public String getName() {
        return name;
    }

    public char getType() {
        return type;
    }

    public int getBlood() {
        return blood;
    }
    

    
    

}
